import boto3
import time
import socket

#import fabric
#from fabric.operations import *
#from fabric.context_managers import *

import redis
import requests


AWS_ACCESS_KEY_ID = 'AKIAJMSVRUHDMP544FFQ'
AWS_SECRET_ACCESS_KEY = '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'

REGION = 'us-east-1'

class Linux:

    vpc = None 
    instance = None
    redis = None

    def __init__(self):
        pass

    def deploy(self):

        ec2 = boto3.resource('ec2', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name=REGION)
        vpc = ec2.create_vpc(CidrBlock='10.0.0.0/16', AmazonProvidedIpv6CidrBlock=False)

        while vpc.state != "available":
        	time.sleep(0.25)
        	vpc.reload()

        s0 = vpc.create_subnet(CidrBlock='10.0.0.0/24', AvailabilityZone='us-east-1e')

        internet_gateway = ec2.create_internet_gateway()
        internet_gateway.attach_to_vpc(VpcId=vpc.vpc_id)

        route_tables = list(vpc.route_tables.all())
        route = route_tables[0].create_route( DestinationCidrBlock='0.0.0.0/0', GatewayId=internet_gateway.id)

        sg = vpc.create_security_group(GroupName="sample-name", Description="A sample description")

        ip_ranges = [{
        	'CidrIp': '0.0.0.0/0'
        }]

        perms = [{
            'IpProtocol': 'TCP',
            'FromPort': 80,
            'ToPort': 80,
            'IpRanges': ip_ranges,
        }, {
            'IpProtocol': 'TCP',
            'FromPort': 443,
            'ToPort': 443,
            'IpRanges': ip_ranges,
        }, {
            'IpProtocol': 'TCP',
            'FromPort': 6379,
            'ToPort': 6379,
            'IpRanges': ip_ranges,
        }, {
            'IpProtocol': 'TCP',
            'FromPort': 22,
            'ToPort': 22,
            'IpRanges': ip_ranges, 
        }]

        sg.authorize_ingress(IpPermissions=perms)

        #defaults
        kwargs = {}
        kwargs['MinCount'] = kwargs.get('MinCount', 1) # required
        kwargs['MaxCount'] = kwargs.get('MaxCount', 1) # required
        kwargs['InstanceType'] = kwargs.get('InstanceType', 't2.micro') # required

        #kwargs['ImageId'] = kwargs.get('ImageId', 'ami-0fa5af19')       # redis 4.0
        kwargs['ImageId'] = kwargs.get('ImageId', 'ami-0b33d91d')       # Amz Linux
        kwargs['KeyName'] = kwargs.get('KeyName', 'ec2-demo-key')       # required

        nics = [ {'SubnetId':s0.id, 'DeviceIndex':0, 'AssociatePublicIpAddress': True, 'Groups':[ sg.id ], } ]
        instances = ec2.create_instances(  NetworkInterfaces=nics, Placement={'AvailabilityZone': 'us-east-1e'}, **kwargs )

        self.instance = instances[0]

        self.instance.wait_until_running()
        self.instance.reload()

        #print( instance.public_ip_address )

    def wait_until_ssh_ready(self, timeout=90):

        while True:

            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(2)                              
            result = sock.connect_ex((self.instance.public_ip_address, 22))
            if result == 0:
                #print('OPEN')
                break
            else:
                #print( 'CLOSED, errno = ' + str(result) )
                time.sleep(5)

    def terminate(self):

        self.instance.terminate()

